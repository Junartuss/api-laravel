<?php

namespace App\Http\Controllers;

use App\Http\Requests\TagAddRequest;
use App\Http\Resources\Tag\TagCollection;
use App\Http\Resources\Tag\TagResource;
use App\Tag;
use Illuminate\Http\Request;

class TagController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return new TagCollection(Tag::paginate());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(TagAddRequest $request)
    {
        $tag = Tag::create($request->all());
        return new TagResource($tag);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $tag = Tag::with('user', 'articles')->get()->find($id);
        if($tag != null){
            return new TagResource($tag);
        }
        return response()->json([
            'success' => false,
            'message' => 'Tag introuvable'
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(TagAddRequest $request, $id)
    {
        $tag = Tag::find($id);
        if($tag != null){
            $tag->update($request->all());
            return new TagResource($tag);
        }
        return response()->json([
            'success' => false,
            'message' => 'Tag introuvable'
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $tag = Tag::find($id);
        if($tag != null){
            $tag->delete();
            return response()->json([
                'success' => true,
                'message' => 'Le tag a bien été supprimer',
            ]);
        }
        return response()->json([
            'success' => false,
            'message' => 'Tag introuvable'
        ]);
    }
}
