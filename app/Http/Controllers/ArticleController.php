<?php

namespace App\Http\Controllers;

use App\Article;
use App\Comment;
use App\Http\Requests\ArticleAddRequest;
use App\Http\Resources\Article\ArticleCollection;
use App\Http\Resources\Article\ArticleResource;
use App\Http\Resources\Comment\CommentCollection;
use App\Http\Resources\Comment\CommentResource;
use Illuminate\Http\Request;
use Illuminate\Validation\Validator;

class ArticleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return new ArticleCollection(Article::paginate());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ArticleAddRequest $request)
    {
        $article = Article::create($request->all());
        $article->tags()->attach($request->tags);
        return new ArticleResource($article);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $article = Article::with('user', 'category', 'tags')->get()->find($id);
        if($article != null){
            return new ArticleResource($article);
        }
        return response()->json([
            'success' => false,
            'message' => 'Article introuvable'
        ]);
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ArticleAddRequest $request, $id)
    {
        $article = Article::find($id);
        if($article != null){
            $article->tags()->sync($request->tags);
            $article->update($request->all());
            return new ArticleResource($article);
        }
        return response()->json([
            'success' => false,
            'message' => 'Article introuvable'
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $article = Article::find($id);
        if($article != null){
            $article->comments()->delete();
            $article->tags()->detach($article->tags);
            $article->delete();
            return response()->json([
                'success' => true,
                'message' => 'L\'article a bien été supprimer',
            ]);
        }
        return response()->json([
            'success' => false,
            'message' => 'Article introuvable'
        ]);
    }
}
