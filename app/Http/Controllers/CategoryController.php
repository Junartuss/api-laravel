<?php

namespace App\Http\Controllers;

use App\Category;
use App\Http\Requests\CategoryAddRequest;
use App\Http\Resources\Category\CategoryCollection;
use App\Http\Resources\Category\CategoryResource;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return new CategoryCollection(Category::paginate());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CategoryAddRequest $request)
    {
        $category = Category::create($request->all());
        return new CategoryResource($category);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $category = Category::with('articles')->get()->find($id);
        if($category != null){
            return new CategoryResource($category);
        }
        return response()->json([
            'success' => false,
            'message' => 'Catégorie introuvable'
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CategoryAddRequest $request, $id)
    {
        $category = Category::find($id);
        if($category != null){
            $category->update($request->all());
            return new CategoryResource($category);
        }
        return response()->json([
            'success' => false,
            'message' => 'Catégorie introuvable'
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $category = Category::find($id);
        if($category != null){
            $category->delete();
            return response()->json([
                'success' => true,
                'message' => 'La catégorie a bien été supprimer',
            ]);
        }
        return response()->json([
            'success' => false,
            'message' => 'Catégorie introuvable'
        ]);
    }
}
