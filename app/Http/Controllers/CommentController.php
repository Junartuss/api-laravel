<?php

namespace App\Http\Controllers;

use App\Article;
use App\Comment;
use App\Http\Requests\CommentAddRequest;
use App\Http\Resources\Comment\CommentCollection;
use App\Http\Resources\Comment\CommentResource;
use Illuminate\Http\Request;

class CommentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($idarticle)
    {
        $article = Article::with('comments')->get()->find($idarticle);
        if($article != null){
            return new CommentCollection($article->comments);
        }
        return response()->json([
            'success' => false,
            'message' => 'Article introuvable'
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CommentAddRequest $request, $idarticle)
    {
        $article = Article::find($idarticle);
        if($article != null){
            $comment = Comment::create($request->all());
            $comment->setAttribute('article_id', $idarticle)->save();
            return new CommentResource($comment);
        }
        return response()->json([
            'success' => false,
            'message' => 'Article introuvable'
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($article, $comment)
    {
        $commentObject = Comment::with('user')->where('article_id', $article)->get()->find($comment);
        if($commentObject != null){
            return new CommentResource($commentObject);
        }
        return response()->json([
            'success' => false,
            'message' => 'Commentaire introuvable'
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
