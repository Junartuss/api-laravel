<?php

namespace App\Http\Resources\Article;

use App\Http\Resources\Tag\TagCollection;
use Illuminate\Http\Resources\Json\JsonResource;
use TijsVerkoyen\CssToInlineStyles\Css\Property\Processor;

class ArticleResource extends JsonResource
{

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'content' => $this->content,
            'user' => ($this->user != null) ? $this->user->permalink : null,
            'category' => ($this->category != null) ? $this->category->permalink : null,
            'comments' => route('articles.comments.index', [$this->id]),
            'tags' => new TagCollection($this->tags),
        ];
    }

    public function with($request)
    {
        return [
            'api_version' => '1.0'
        ];
    }
}
