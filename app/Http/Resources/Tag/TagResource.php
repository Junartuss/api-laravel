<?php

namespace App\Http\Resources\Tag;

use App\Http\Resources\Article\ArticleCollection;
use Illuminate\Http\Resources\Json\JsonResource;

class TagResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'user' => ($this->user != null) ? $this->user->permalink : null,
            'articles' => new ArticleCollection($this->articles)
        ];
    }
}
