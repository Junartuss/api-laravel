<?php

namespace App\Http\Resources\Category;

use App\Category;
use Illuminate\Http\Resources\Json\ResourceCollection;

class CategoryCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'data' =>
                $this->collection->transform(function (Category $category) {
                    return [
                        'title' => $category->title,
                        'url' => $category->permalink
                    ];
                }),
        ];
    }
}
