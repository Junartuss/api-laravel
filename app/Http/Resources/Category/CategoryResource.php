<?php

namespace App\Http\Resources\Category;

use App\Article;
use App\Http\Resources\Article\ArticleCollection;
use Illuminate\Http\Resources\Json\JsonResource;

class CategoryResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'articles' => new ArticleCollection($this->articles)
        ];
    }
}
