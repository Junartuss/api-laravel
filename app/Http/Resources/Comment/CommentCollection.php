<?php

namespace App\Http\Resources\Comment;

use App\Comment;
use Illuminate\Http\Resources\Json\ResourceCollection;

class CommentCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'data' =>
                $this->collection->transform(function (Comment $comment) {
                    return [
                        'id' => $comment->id,
                        'url' => $comment->permalink
                    ];
                }),
        ];
    }
}
