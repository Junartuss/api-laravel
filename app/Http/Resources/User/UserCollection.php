<?php

namespace App\Http\Resources\User;

use App\User;
use Illuminate\Http\Resources\Json\ResourceCollection;

class UserCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'data' =>
                $this->collection->transform(function (User $user) {
                    return [
                        'name' => $user->name,
                        'url' => $user->permalink
                    ];
                }),
        ];
    }
}
