<?php

namespace App\Http\Resources\User;

use App\Http\Resources\Article\ArticleCollection;
use App\Http\Resources\Comment\CommentCollection;
use App\Http\Resources\Tag\TagCollection;
use Illuminate\Http\Resources\Json\JsonResource;
use TijsVerkoyen\CssToInlineStyles\Css\Property\Processor;

class UserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'email' => $this->email,
            'articles' => new ArticleCollection($this->articles),
            'comments' => new CommentCollection($this->comments),
            'tags' => new TagCollection($this->tags)
        ];
    }
}
