<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Category extends Model
{
    protected $fillable = ['title'];

    public function articles(){
        return $this->hasMany('App\Article');
    }

    public function setTitleAttribute($value){
        $this->attributes['slug'] = Str::slug($value);
        $this->attributes['title'] = $value;
    }

    public function getPermalinkAttribute(){
        return route('categories.show', $this->id);
    }

    public function getCreatedAtStringAttribute()
    {
        return Carbon::parse($this->created_at)->format('d/m/Y');
    }

    public function getUpdatedAtStringAttribute()
    {
        return Carbon::parse($this->updated_at)->format('d/m/Y');
    }
}
