<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Tag extends Model
{
    protected $fillable = ['title', 'user_id'];

    public function user(){
        return $this->belongsTo('App\User');
    }

    public function articles(){
        return $this->belongsToMany('App\Article');
    }

    public function reports()
    {
        return $this->morphMany('App\Report', 'reportable');
    }

    public function getPermalinkAttribute(){
        return route('tags.show', $this->id);
    }

    public function setTitleAttribute($value){
        $this->attributes['slug'] = Str::slug($value);
        $this->attributes['title'] = $value;
    }

    public function getCreatedAtStringAttribute()
    {
        return Carbon::parse($this->created_at)->format('d/m/Y');
    }

    public function getUpdatedAtStringAttribute()
    {
        return Carbon::parse($this->updated_at)->format('d/m/Y');
    }
}
