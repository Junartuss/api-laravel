<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'active', 'renew', 'role_id', 'subcription_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function articles(){
        return $this->hasMany('App\Article');
    }

    public function tags(){
        return $this->hasMany('App\Tag');
    }

    public function role(){
        return $this->belongsTo('App\Role');
    }

    public function comments()
    {
        return $this->hasMany('App\Comment');
    }

    public function reports()
    {
        return $this->hasMany('App\Report');
    }

    public function threads()
    {
        return $this->hasMany('App\Thread');
    }

    public function messages()
    {
        return $this->hasMany('App\Message');
    }

    public function invoiceAddresses()
    {
        return $this->hasMany('App\InvoiceAddresse');
    }

    public function invoices()
    {
        return $this->hasMany('App\Invoice');
    }

    public function subcription()
    {
        return $this->belongsTo('App\Subcription');
    }

    public function getPermalinkAttribute(){
        return route('users.show', $this->id);
    }

    public function getStringSubcriptionAttribute()
    {
        return (!empty($this->subcription) ? "Abonnement " . $this->subcription->duration . " mois" : "Pas d'abonnement");
    }

    public function getStringActiveAttribute()
    {
        return ($this->active) ? "Oui" : "Non";
    }

    public function getStringRenewAttribute()
    {
        return ($this->renew) ? "Oui" : "Non";
    }

    public function getStringDateExpirationAttribute()
    {
        return Carbon::parse($this->date_expiration)->format('d/m/Y');
    }

    public function hasRole($role)
    {
        switch ($role){
            case 'administrateur':
                if($this->role->title == $role){
                    return true;
                }
                return false;
                break;
            case 'editeur':
                if(in_array($this->role->title, ['administrateur', 'editeur'])){
                    return true;
                }
                return false;
                break;
            case 'premium':
                if(in_array($this->role->title, ['administrateur', 'editeur', 'premium'])){
                    return true;
                }
                return false;
                break;
            case 'utilisateur':
                if(in_array($this->role->title, ['administrateur', 'editeur', 'premium', 'utilisateur'])){
                    return true;
                }
                return false;
                break;
            default:
                return false;
                break;
        }
    }
}
