<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InvoiceAddresse extends Model
{
    protected $fillable = ['address', 'zip_code', 'city', 'country', 'user_id'];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function invoices()
    {
        return $this->hasMany('App\Invoice');
    }
}
