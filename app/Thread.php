<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Thread extends Model
{
    protected $fillable = ['title'];

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }

    public function setTitleAttribute($value){
        $this->attributes['slug'] = Str::slug($value);
        $this->attributes['title'] = $value;
    }

    public function user(){
        return $this->belongsTo('App\User');
    }

    public function messages()
    {
        return $this->hasMany('App\Message');
    }

    public function reports()
    {
        return $this->morphMany('App\Report', 'reportable');
    }

    public function getCreatedAtStringAttribute()
    {
        return Carbon::parse($this->created_at)->format('d/m/Y');
    }

    public function getUpdatedAtStringAttribute()
    {
        return Carbon::parse($this->updated_at)->format('d/m/Y');
    }
}
