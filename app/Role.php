<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    public function users(){
        return $this->belongsToMany('App\User');
    }

    public function getLabelColorAttribute()
    {
        switch ($this->title){
            case 'administrateur':
                return '<span class="text-danger">' . $this->label . '</span>';
                break;
            case 'editeur':
                return '<span class="text-warning">' . $this->label . '</span>';
                break;
            case 'premium':
                return '<span class="text-info">' . $this->label . '</span>';
                break;
            default:
                return '<span class="text-secondary">' . $this->label . '</span>';
                break;
        }
    }
}
