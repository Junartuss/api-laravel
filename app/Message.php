<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    protected $fillable = ['content', 'thread_id'];

    public function thread(){
        return $this->belongsTo('App\Thread');
    }

    public function user(){
        return $this->belongsTo('App\User');
    }

    public function reports()
    {
        return $this->morphMany('App\Report', 'reportable');
    }

    public function getCreatedAtStringAttribute()
    {
        return Carbon::parse($this->created_at)->format('d/m/Y');
    }

    public function getUpdatedAtStringAttribute()
    {
        return Carbon::parse($this->updated_at)->format('d/m/Y');
    }

}
