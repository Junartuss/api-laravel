<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Article extends Model
{
    protected $fillable = ['title', 'premium', 'content', 'published', 'user_id', 'category_id'];

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }

    public function category()
    {
        return $this->belongsTo('App\Category');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function tags()
    {
        return $this->belongsToMany('App\Tag');
    }

    public function comments()
    {
        return $this->hasMany('App\Comment');
    }

    public function reports()
    {
        return $this->morphMany('App\Report', 'reportable');
    }

    public function getPermalinkAttribute(){
        return route('articles.show', $this->id);
    }

    public function setTitleAttribute($value){
        $this->attributes['slug'] = Str::slug($value);
        $this->attributes['title'] = $value;
    }

    function getExcerpt($startPos=0, $maxLength=100) {
        if(strlen($this->content) > $maxLength) {
            $excerpt   = substr($this->content, $startPos, $maxLength-3);
            $lastSpace = strrpos($excerpt, ' ');
            $excerpt   = substr($excerpt, 0, $lastSpace);
            $excerpt  .= '...';
        } else {
            $excerpt = $this->content;
        }
        return $excerpt;
    }

    public function getPremiumStringAttribute()
    {
        return ($this->premium) ? 'Oui' : 'Non';
    }

    public function getPublishedStringAttribute()
    {
        return ($this->published) ? 'Publié' : 'Non publié';
    }

    public function getCreatedAtStringAttribute()
    {
        return Carbon::parse($this->created_at)->format('d/m/Y');
    }

    public function getUpdatedAtStringAttribute()
    {
        return Carbon::parse($this->updated_at)->format('d/m/Y');
    }

}
