<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    protected $fillable = ['content', 'article_id', 'user_id'];

    public function article()
    {
        return $this->belongsTo('App\Article');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function reports()
    {
        return $this->morphMany('App\Report', 'reportable');
    }

    public function getPermalinkAttribute(){
        return route('articles.comments.show', [$this->article->id, $this->id]);
    }

    public function getCreatedAtStringAttribute()
    {
        return Carbon::parse($this->created_at)->format('d/m/Y');
    }
}
