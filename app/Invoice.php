<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Invoice extends Model
{
    protected $fillable = ['status'];

    public function invoiceAddresse()
    {
        return $this->belongsTo('App\InvoiceAddresse');
    }

    public function invoiceLines()
    {
        return $this->hasMany('App\InvoiceLine');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function getStringStatusAttribute()
    {
        switch ($this->status){
            case "validated":
                return "Validé";
                break;
            case "processing":
                return "En cours";
                break;
            case "canceled":
                return "Annulé";
                break;
            default:
                return "En cours";
                break;
        }
    }

    public static function getStringStatus()
    {
        return [
            'validated' => 'Validé',
            'processing' => 'En cours',
            'canceled' => 'Annulé'
        ];
    }

    public function getCreatedAtStringAttribute()
    {
        return Carbon::parse($this->created_at)->format('d/m/Y');
    }

    public function getUpdatedAtStringAttribute()
    {
        return Carbon::parse($this->updated_at)->format('d/m/Y');
    }

    public static function randomNumber($length) {
        $result = '';
        for($i = 0; $i < $length; $i++) {
            $result .= mt_rand(0, 9);
        }
        return $result;
    }
}
