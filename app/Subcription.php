<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subcription extends Model
{
    protected $fillable = ['price', 'duration'];

    public function users()
    {
        return $this->hasMany('App\User');
    }

    public function getStringSubcriptionAttribute()
    {
        return "Abonnement " . $this->duration . " mois";
    }

    public static function getStringSubcription()
    {
        $result = [];
        $result[0] = "Pas d'abonnement";
        foreach (Subcription::all() as $subcription)
        {
            $result[$subcription->id] = "Abonnement " . $subcription->duration . " mois";
        }
        return $result;
    }
}
