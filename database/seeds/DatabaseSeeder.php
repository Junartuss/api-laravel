<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Category::class, 5)->create();
        factory(App\User::class, 10)->create();
        factory(App\Tag::class, 10)->make()->each(function (App\Tag $tag){
            $tag->user()->associate(App\User::inRandomOrder('')->first())->save();
        });;
        factory(App\Article::class, 50)->make()->each(function (App\Article $article){
            $article->category()->associate(App\Category::inRandomOrder('')->first());
            $article->user()->associate(App\User::inRandomOrder('')->first())->save();
            $article->tags()->attach(App\Tag::inRandomOrder('')->take(5)->get());
            $article->save();
        });
        factory(App\Comment::class, 200)->make()->each(function (App\Comment $comment){
            $comment->article()->associate(App\Article::inRandomOrder('')->first());
            $comment->user()->associate(App\User::inRandomOrder('')->first());
            $comment->save();
        });

        factory(App\Thread::class, 10)->make()->each(function (App\Thread $thread){
            $thread->user()->associate(App\User::inRandomOrder('')->first())->save();
        });
        factory(App\Message::class, 50)->make()->each(function (App\Message $message){
            $message->user()->associate(App\User::inRandomOrder('')->first())->save();
            $message->thread()->associate(App\Thread::inRandomOrder('')->first())->save();
        });

        factory(App\InvoiceAddresse::class, 30)->make()->each(function (App\InvoiceAddresse $invoiceAddresse){
            $invoiceAddresse->user()->associate(App\User::InRandomOrder('')->first())->save();
        });

        factory(App\Invoice::class, 30)->make()->each(function (App\Invoice $invoice){
            $invoice->user()->associate(App\User::InRandomOrder('')->first())->save();
            $invoice->invoiceAddresse()->associate(App\InvoiceAddresse::InRandomOrder('')->first())->save();
        });

        factory(App\InvoiceLine::class, 100)->make()->each(function (App\InvoiceLine $invoiceLine){
            $invoiceLine->invoice()->associate(App\Invoice::InRandomOrder('')->first())->save();
            $invoiceLine->subcription()->associate(App\Subcription::InRandomOrder('')->first())->save();
        });

        $this->addPriceQuantityInvoice();

        // COMPTE ADMIN
        DB::table('users')->insert(
            [
                0 => [
                    'name' => 'Compte Admin',
                    'email' => 'admin@admin.fr',
                    'password' => \Illuminate\Support\Facades\Hash::make('admin'),
                    'role_id' => 4,
                ],
            ]
        );

        // COMPTE PREMIUM
        DB::table('users')->insert(
            [
                0 => [
                    'name' => 'Compte Premium',
                    'email' => 'premium@premium.fr',
                    'password' => \Illuminate\Support\Facades\Hash::make('premium'),
                    'date_expiration' => \Carbon\Carbon::now()->addMonths(1),
                    'role_id' => 2,
                    'subcription_id' => 1
                ],
            ]
        );

        // COMPTE UTILISATEUR
        DB::table('users')->insert(
            [
                0 => [
                    'name' => 'Compte Utilisateur',
                    'email' => 'utilisateur@utilisateur.fr',
                    'password' => \Illuminate\Support\Facades\Hash::make('utilisateur'),
                    'role_id' => 1,
                ],
            ]
        );

    }

    public function addPriceQuantityInvoice()
    {
        $invoices = \App\Invoice::all();

        foreach ($invoices as $invoice){
            $total_price = 0;
            $total_quantity = 0;

            foreach ($invoice->invoiceLines as $invoiceLine)
            {
                $total_price += $invoiceLine->price;
                $total_quantity += $invoiceLine->quantity;
            }

            $invoice->update([
                'total_price' => $total_price,
                'total_quantity' => $total_quantity
            ]);
        }

        return true;
    }
}
