<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Invoice;
use Faker\Generator as Faker;


$factory->define(Invoice::class, function (Faker $faker) {

    $arrayStatus = ['validated', 'processing', 'canceled'];
    $choice = array_rand($arrayStatus, 1);

    return [
        'invoice_number' => $faker->ean8(),
        'total_price' => $faker->randomNumber(2),
        'total_quantity' => $faker->randomNumber(2),
        'status' => $arrayStatus[$choice],
        'payment_reference' => $faker->text(5)
    ];
});
