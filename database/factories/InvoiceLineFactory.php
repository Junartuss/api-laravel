<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\InvoiceLine;
use Faker\Generator as Faker;

$factory->define(InvoiceLine::class, function (Faker $faker) {
    return [
        'price' => $faker->randomNumber(2),
        'quantity' => $faker->randomNumber(2),
    ];
});
