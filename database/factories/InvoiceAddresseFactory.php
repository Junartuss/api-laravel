<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\InvoiceAddresse;
use Faker\Generator as Faker;

$factory->define(InvoiceAddresse::class, function (Faker $faker) {
    return [
        'address' => $faker->address(),
        'zip_code' => $faker->postcode(),
        'city' => $faker->city(),
        'country' => $faker->country()
    ];
});
