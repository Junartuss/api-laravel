<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

class CreateRolesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('roles', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('title');
            $table->string('label');
        });

        DB::table('roles')->insert(
            [
                0 => ['title' => 'utilisateur', 'label' => 'Utilisateur'],
                1 => ['title' => 'premium', 'label' => 'Premium'],
                2 => ['title' => 'editeur', 'label' => 'Éditeur'],
                3 => ['title' => 'administrateur', 'label' => 'Administrateur']
            ]
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('roles');
    }
}
