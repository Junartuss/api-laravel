<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

class CreateSubcriptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('subcriptions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->float('price');
            $table->integer('duration');
            $table->timestamps();
        });

        DB::table('subcriptions')->insert(
            [
                0 => ['price' => '2', 'duration' => 1],
                1 => ['price' => '10', 'duration' => 6],
                2 => ['price' => '16', 'duration' => 12],
            ]
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('subcriptions');
    }
}
