<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::apiResource('articles', 'ArticleController');

Route::group(['prefix' => 'articles'],function(){
    Route::get('/{article}/comments',[
        'uses' => 'CommentController@index',
        'as' => 'articles.comments.index',
    ]);
    Route::post('/{article}/comments',[
        'uses' => 'CommentController@store',
        'as' => 'articles.comments.store',
    ]);
    Route::get('/{article}/comments/{comment}',[
        'uses' => 'CommentController@show',
        'as' => 'articles.comments.show',
    ]);
});

Route::apiResource('users', 'UserController');

Route::apiResource('categories', 'CategoryController');

Route::apiResource('tags', 'TagController');
